﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace OptimalPrime
{

    class OnlineAlgorithm
    {
        public static bool[,] grid;
        public static int theaterX, theaterY;
        static void Main(string[] args)
        {
            //Reading of the start grid starts here
            int maxgroupsize = 8;
            int rows = int.Parse(Console.ReadLine());
            theaterX = rows;
            int m = int.Parse(Console.ReadLine());
            theaterY = m;
            grid = new bool[rows, m];
            string[] string_grid = new string[rows];

            for (int i = 0; i < rows; i++)
            {
                string row = Console.ReadLine();
                string_grid[i] = row;
                for (int j = 0; j < m; j++)
                {
                    grid[i, j] = row[j] == '1';
                }
            }
            List<(int, int)>[] occurencelist = new List<(int, int)>[maxgroupsize];

            for (int i = maxgroupsize - 1; i >= 0; i--)
            {
                string pattern = new String('1', i + 1);
                List<(int, int)> patterns = FindOccurences(string_grid, pattern);
                occurencelist[i] = patterns;
            }
            //Reading of the start grid ends here


            ///     Below the scoring function for assigning seats can be specified.
            ///     Possible choices are:
            ///         = TopLeft
            ///         = SpaceScore
            ///         = OccupationScore
            ///         = SpaceOccupationScore
            ///         = Random3SpaceOccupationScore
            ///         = BestRandomSpaceOccupationScore
            ///     Any of those can be placed in the line below in the following
            ///     format with FUNCTION replaced with the terms above:
            ///         Console.WriteLine(FUNCTION(occurencelist))

            Console.WriteLine(SpaceOccupationScore(occurencelist));
        }
        //Method that "places" the groups randomly at one of the seats with the lowest possible score
        //according to the space preservation * 50 + occupation strategy and returns the total amount of people placed
        static int BestRandomSpaceOccupationScore(List<(int, int)>[] occurencelist)
        {
            int nextgroup = int.Parse(Console.ReadLine());
            int totalpeople = 0;

            while (nextgroup != 0)
            {

                List<(int, int)> bestplaces = new List<(int, int)> { (0, 0) };
                List<int> bestvals = new List<int> { int.MaxValue };
                int loopnumber = Math.Min(1000, occurencelist[nextgroup - 1].Count);
                for (int i = 0; i < loopnumber; i++)
                {
                    (int, int) coords = occurencelist[nextgroup - 1][i];
                    int curr = (TrySpace(coords.Item1, coords.Item2, nextgroup, occurencelist) * 50) + TryOccupy(coords.Item1, coords.Item2, nextgroup, occurencelist);

                    
                    if( curr == bestvals[0])
                    {
                        bestvals.Add(curr);
                        bestplaces.Add(coords);
                    }


                    if (curr < bestvals[0])
                    {
                        bestvals.Clear();
                        bestplaces.Clear();
                        bestvals.Add(curr);
                        bestplaces.Add(coords);
                    }
                    

                }

                if (bestvals.Count > 0 && bestvals[0] < int.MaxValue)
                {
                    int r = new Random().Next(bestvals.Count);
                    PlaceGroupGrid(bestplaces[r].Item1, bestplaces[r].Item2, nextgroup, occurencelist);

                    totalpeople += nextgroup;

                    Console.WriteLine((bestplaces[r].Item1+1) + " " + (bestplaces[r].Item2+1));
                }
                else
                {
                    Console.WriteLine("0 0");
                }



                nextgroup = int.Parse(Console.ReadLine());
            }

            return totalpeople;
        }
        //Method that "places" the groups randomly at one of the three seats with the lowest possible score 
        //according to the space preservation * 50 + occupation strategy and returns the total amount of people placed
        static int Random3SpaceOccupationScore(List<(int, int)>[] occurencelist)
        {
            int nextgroup = int.Parse(Console.ReadLine());
            int totalpeople = 0;

            while (nextgroup != 0)
            {

                List<(int, int)> bestplaces = new List<(int, int)>{ (0, 0), (0, 0), (0, 0) };
                List<int> bestvals = new List<int>{ int.MaxValue, int.MaxValue, int.MaxValue };
                int loopnumber = Math.Min(1000, occurencelist[nextgroup - 1].Count);
                for (int i = 0; i < loopnumber; i++)
                {
                    (int, int) coords = occurencelist[nextgroup - 1][i];
                    int curr = (TrySpace(coords.Item1, coords.Item2, nextgroup, occurencelist) * 50) + (TryOccupy(coords.Item1, coords.Item2, nextgroup, occurencelist)*0);

                    int curry = bestvals.Max();
                    if (curr < curry)
                    {
                        int plaatsie = bestvals.IndexOf(curry);
                        bestvals[plaatsie] = curr;
                        bestplaces[plaatsie] = coords;
                    }
                    
                }
                for (int j = 2; j >= 0; j--)
                {
                    if (int.MaxValue == bestvals[j])
                    {
                        bestplaces.RemoveAt(j);
                        bestvals.RemoveAt(j);
                    }
                }
                
                if (bestvals.Count > 0 && bestvals[0] < int.MaxValue)
                {
                    int r = new Random().Next(bestvals.Count);
                    PlaceGroupGrid(bestplaces[r].Item1, bestplaces[r].Item2, nextgroup, occurencelist);
                    totalpeople += nextgroup;
                    Console.WriteLine((bestplaces[r].Item1+1) + " " + (bestplaces[r].Item2+1));
                }
                else
                {
                    Console.WriteLine("0 0");
                }

                

                nextgroup = int.Parse(Console.ReadLine());
            }

            return totalpeople;
        }
        //Method that "places" the groups at the seats with the lowest possible score according to the space preservation * 50 + occupation strategy
        //and returns the total amount of people placed
        static int SpaceOccupationScore(List<(int, int)>[] occurencelist)
        {
            int nextgroup = int.Parse(Console.ReadLine());
            int totalpeople = 0;

            while (nextgroup != 0)
            {

                (int, int) bestplace = (-1, -1);
                int bestval = int.MaxValue;
                int loopnumber = Math.Min(1000, occurencelist[nextgroup - 1].Count);
                for (int i = 0; i < loopnumber; i++)
                {
                    (int, int) coords = occurencelist[nextgroup - 1][i];
                    int curr = (TrySpace(coords.Item1, coords.Item2, nextgroup, occurencelist)*50)+ TryOccupy(coords.Item1, coords.Item2, nextgroup, occurencelist);
                    if (curr < bestval)
                    {
                        bestplace = coords;
                        bestval = curr;
                    }
                }
                if (bestval != int.MaxValue)
                {
                    PlaceGroupGrid(bestplace.Item1, bestplace.Item2, nextgroup, occurencelist);
                    totalpeople += nextgroup;
                }
                
                Console.WriteLine((bestplace.Item1+1) + " " + (bestplace.Item2+1));
                

                nextgroup = int.Parse(Console.ReadLine());
            }

            return totalpeople;
        }
        //Method that "places" the groups at the seats with the lowest possible score according to the space preservation strategy
        //and returns the total amount of people placed
        static int SpaceScore(List<(int, int)>[] occurencelist)
        {
            int nextgroup = int.Parse(Console.ReadLine());
            int totalpeople = 0;

            while (nextgroup != 0)
            {

                (int, int) bestplace = (-1, -1);
                int bestval = int.MaxValue;
                int loopnumber = Math.Min(1000, occurencelist[nextgroup - 1].Count);
                for (int i = 0; i < loopnumber; i++)
                {
                    (int, int) coords = occurencelist[nextgroup - 1][i];
                    int curr = TrySpace(coords.Item1, coords.Item2, nextgroup, occurencelist);
                    if (curr < bestval)
                    {
                        bestplace = coords;
                        bestval = curr;
                    }
                }
                if (bestval != int.MaxValue)
                {
                    PlaceGroupGrid(bestplace.Item1, bestplace.Item2, nextgroup, occurencelist);
                    totalpeople += nextgroup;
                }

                Console.WriteLine((bestplace.Item1+1) + " " + (bestplace.Item2+1));

                nextgroup = int.Parse(Console.ReadLine());
            }
            
            return totalpeople;
        }
        //Method that "places" the groups at the seats with the lowest possible score according to the occupation strategy
        //and returns the total amount of people placed
        static int OccupationScore(List<(int, int)>[] occurencelist)
        {
            int nextgroup = int.Parse(Console.ReadLine());
            int totalpeople = 0;
            
            while (nextgroup != 0)
            {
                (int, int) bestplace = (-1, -1);
                int bestval = int.MaxValue;
                int loopnumber = Math.Min(1000, occurencelist[nextgroup - 1].Count);
                for(int i = 0; i < loopnumber; i++)
                {
                    (int, int) coords = occurencelist[nextgroup - 1][i];
                    int curr = TryOccupy(coords.Item1, coords.Item2, nextgroup, occurencelist);
                    if(curr < bestval)
                    {
                        bestplace = coords;
                        bestval = curr;
                    }
                }
                if(bestval != int.MaxValue)
                {
                    PlaceGroup(bestplace.Item1, bestplace.Item2, nextgroup, occurencelist);
                    totalpeople += nextgroup;
                }
                Console.WriteLine((bestplace.Item1+1) + " " + (bestplace.Item2+1));
                nextgroup = int.Parse(Console.ReadLine());
            }
            return totalpeople;
        }
        //Method that "places" the groups at the top-left most possible spot and returns the total amount of people placed
        static int TopLeft(List<(int,int)>[] occurencelist)
        {
            int nextgroup = int.Parse(Console.ReadLine());
            int totalpeople = 0;

            while (nextgroup != 0)
            {
                (int, int) coords;
                try
                { 
                    coords = occurencelist[nextgroup - 1].First();
                    PlaceGroup(coords.Item1, coords.Item2, nextgroup, occurencelist);
                    totalpeople += nextgroup;
                    Console.WriteLine((coords.Item1+1) + " " + (coords.Item2+1));
                }
                catch
                {
                    Console.WriteLine("0 0");
                }
                nextgroup = int.Parse(Console.ReadLine());
            }
            return totalpeople;
        }
        //Method that "places" a group by updating the occurencelist by deleting entries made 
        //invalid by placing the specified group at the specified location
        static void PlaceGroup(int x, int y, int groupsize, List<(int,int)>[] occurencelist)
        {
            for (int n = 0; n < occurencelist.Length; n++)
            {
                for (int i = x - 1; i <= x + 1; i++)
                {
                    for (int j = y - 2 - n; j <= y + 2 + groupsize - 1; j++)
                    {
                        if((i == x-1 || i == x+1) && (j == y-2-n || j == y + 2 + groupsize - 1))
                        {
                            continue;
                        }
                        occurencelist[n].Remove((i, j));
                    }
                }
            }
        }
        //Method that "places" a group by updating the occurencelist by deleting entries made 
        //invalid and updating the boolean grid by making squares false that can no longer be sat on 
        //by placing the specified group at the specified location
        static void PlaceGroupGrid(int x, int y, int groupsize, List<(int, int)>[] occurencelist)
        {
            for (int n = 0; n < occurencelist.Length; n++)
            {
                for (int i = x - 1; i <= x + 1; i++)
                {
                    for (int j = y - 2 - n; j <= y + 2 + groupsize - 1; j++)
                    {
                        if ((i == x - 1 || i == x + 1) && (j == y - 2 - n || j == y + 2 + groupsize - 1))
                        {
                            continue;
                        }
                        occurencelist[n].Remove((i, j));
                    }
                }
            }
            for(int i = x -1; i <= x + 1; i++)
            {
                for(int j = y - 2; j <= y + 2 + groupsize - 1; j++)
                {
                    if ((i == x - 1 || i == x + 1) && (j == y - 2 || j == y + 2 + groupsize - 1))
                    {
                        continue;
                    }
                    if (i >= 0 && j >=0 && i < theaterX && j < theaterY)
                    {
                        grid[i, j] = false;
                    }
                }
            }
        }
        //Method that returns the cost according to the space strategy of placing a group at the specified location
        static int TrySpace(int x, int y, int groupsize, List<(int, int)>[] occurencelist)
        {
            int ret = 0;

            for (int i = x - 1; i <= x + 1; i++)
            {
                for (int j = y - 2; j <= y + 2 + groupsize - 1; j++)
                {
                    if ((i == x - 1 || i == x + 1) && (j == y - 2 || j == y + 2 + groupsize - 1))
                    {
                        continue;
                    }
                    if (i >= 0 && j >= 0 && i < theaterX && j < theaterY)
                    {
                        if (grid[i, j] == true)
                        {
                            ret++;
                        }
                    }
                }

            }
            return ret;
        }
        //Method that returns the cost according to the occupy strategy of placing a group at the specified location
        static int TryOccupy(int x, int y, int groupsize, List<(int, int)>[] occurencelist)
        {
            int ret = 0;
            for (int n = 0; n < occurencelist.Length; n++)
            {
                for (int i = x - 1; i <= x + 1; i++)
                {
                    for (int j = y - 2 - n; j <= y + 2 + groupsize - 1; j++)
                    {
                        if ((i == x - 1 || i == x + 1) && (j == y - 2 - n || j == y + 2 + groupsize - 1))
                        {
                            continue;
                        }
                        if(occurencelist[n].Contains((i,j)))
                        {
                            ret += (n * 2)+1;
                        }
                    }
                }
            }

            return ret;
        }
        //Method that finds all regex matches in the strings given in grid. it returns the coordinates all occurences of that pattern
        static List<(int, int)> FindOccurences(string[] grid, string pattern)
        {
            pattern = "(?=(" + pattern + "))";
            List<(int, int)> result = new List<(int, int)>();
            for (int i = 0; i < grid.Length; i++)
            {
                string sentence = grid[i];
                Match match = Regex.Match(sentence, pattern);
                while(match.Success){
                    result.Add((i, match.Index));
                    match = match.NextMatch();
                }
            }

            return result;
        }
        //Method that prints the grid
        static void PrintGrid(bool[,] grid)
        {
            Console.WriteLine();
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    Console.Write(grid[i, j] ? '1' : '0');
                }
                Console.WriteLine();
            }
        }
    }
}