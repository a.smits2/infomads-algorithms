using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OptimalPrime
{
    class OfflineAlgorithm
    {
        private static string[] _bestGrid;
        private static int _bestPeople = 1000000;
        private static int _startPeople = 100000;
        private static int[] _bestLeftGroup = new int[8];

        //Timeout time in milliseconds
        private static int timeoutTime = 120 * 1000;
        
        //Using the simple interface that only outputs something after the timeout
        //Turning this false will update live and give you more info about the solution
        private static bool simpleInterface = true;
        
        static void Main(string[] args)
        {
            //Read the input
            int rows = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());

            string[] stringGrid = new string[rows];

            for (int i = 0; i < rows; i++)
            {
                string row = Console.ReadLine();
                stringGrid[i] = row;
            }

            int[] groups = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            _startPeople = CountPeople(groups);
            
            //Find all occurrences
            List<(int, int)>[] occList = new List<(int, int)>[8];
            for (int i = 8 - 1; i >= 0; i--)
            {
                string pattern = new String('1', i + 1);
                List<(int, int)> patterns = FindOccurrences(stringGrid, pattern);
                occList[i] = patterns;
            }
            
            Task.Delay(timeoutTime).ContinueWith(t=> TimeoutCallback());
            Search(occList, groups, stringGrid);
            UpdateConsole();
        }

        static void TimeoutCallback()
        {
            UpdateConsole();
            Environment.Exit(1);
        }
        
        static void Search(List<(int, int)>[] prevlist, int[] prevgroups, string[] prevgrid)
        {
            int remainingPeople = CountPeople(prevgroups);
            if (_bestPeople == 0  || ((_startPeople - remainingPeople) + prevlist[0].Count < _startPeople - _bestPeople))
                return;

            if(remainingPeople < _bestPeople)
            {
                _bestPeople = remainingPeople;
                _bestGrid = prevgrid;
                _bestLeftGroup = prevgroups;
                if(!simpleInterface)
                    UpdateConsole();
            }
            string[] currGrid = CloneGrid(prevgrid);
            List<(int, int)>[] currList = CloneOCC(prevlist);
            int[] currGroups = CloneGroups(prevgroups);

            for (int i = 7; i >= 0; i--)
            {
                if(currList[i].Count <= 0 || currGroups[i] <= 0)
                {
                    continue;
                }
                List<(int, int)>[] nextList = CloneOCC(currList);
                int[] nextGroups = CloneGroups(currGroups);
                string[] nextGrid = CloneGrid(currGrid);

                //Place seats in grid
                int row = currList[i][0].Item1;
                int seat = currList[i][0].Item2;

                char[] charArr = nextGrid[row].ToCharArray();
                for (int j = seat; j <= seat + i; j++)
                    charArr[j] = 'x';
                nextGrid[row] = new string(charArr);

                nextGroups[i] = nextGroups[i] - 1;
                PlaceGroup(i, nextList);

                Search(nextList, nextGroups, nextGrid);
                RemoveFirst(i, currList);
            }
            if (currList[0].Count <= 0)
            {
                return;
            }

            for(int i = 0; i < 8; i++)
            {
                if(currList[i].Count > 0 && currGroups[i] > 0)
                {
                    Search(currList, currGroups, currGrid);
                    return;
                }
            }
        }
        static string[] CloneGrid(string[] grid)
        {
            string[] result = new string[grid.Length];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new string(grid[i]);
            }

            return result;
        }
        static void PrintStringGrid(string[] grid)
        {
            for (int i = 0; i < grid.Length; i++)
            {
                Console.WriteLine(grid[i]);
            }
        }
        static int[] CloneGroups(int[] grps)
        {
            int[] ret = new int[grps.Length];
            for(int i = 0; i < grps.Length; i++)
            {
                ret[i] = grps[i];
            }
            return ret;
        }
        static void RemoveFirst(int grsize, List<(int, int)>[] thislist)
        {
            thislist[grsize].RemoveAt(0);
        }
        static int CountPeople(int[] group)
        {
            int count = 0;
            for (int i = 0; i < group.Length; i++)
            {
                count += (i + 1) * group[i];
            }
            return count;
        }
        static List<(int,int)>[] CloneOCC(List<(int,int)>[] occurs)
        {
            List<(int, int)>[] ret = new List<(int, int)>[occurs.Length];

            for(int i = 0; i < occurs.Length; i++)
            {
                ret[i] = new List<(int, int)>();
                for(int j = 0; j < occurs[i].Count; j++)
                {
                    ret[i].Add((occurs[i][j].Item1, occurs[i][j].Item2));
                }
            }
            return ret;
        }
        static void PlaceGroup(int groupsize, List<(int, int)>[] occurrencelist)
        {
            int x = occurrencelist[groupsize][0].Item1;
            int y = occurrencelist[groupsize][0].Item2;

            for (int n = 0; n < occurrencelist.Length; n++)
            {
                for (int i = x - 1; i <= x + 1; i++)
                {
                    for (int j = y - 2 - n; j <= y + 2 + groupsize; j++)
                    {
                        if ((i == x - 1 || i == x + 1) && (j == y - 2 - n || j == y + 2 + groupsize))
                        {
                            continue;
                        }
                        occurrencelist[n].Remove((i, j));
                    }
                }
            }
        }
        static List<(int, int)> FindOccurrences(string[] grid, string pattern)
        {
            pattern = "(?=(" + pattern + "))";
            List<(int, int)> result = new List<(int, int)>();
            for (int i = 0; i < grid.Length; i++)
            {
                string sentence = grid[i];
                Match match = Regex.Match(sentence, pattern);
                while (match.Success)
                {
                    result.Add((i, match.Index));
                    match = match.NextMatch();
                }
            }

            return result;
        }
        
        static void UpdateConsole()
        {
            if (_bestGrid == null) return;
            if (simpleInterface)
            {
                PrintStringGrid(_bestGrid);
                return;
            }
            
            Console.Clear();
            //Console.WriteLine("Stack size {0}", _stack.Count);
            PrintStringGrid(_bestGrid);
            Console.WriteLine("People left {0}/{1}", _bestPeople, _startPeople);
            Console.WriteLine("Groups left: [{0}]", string.Join(", ", _bestLeftGroup));
        }
    }
}
