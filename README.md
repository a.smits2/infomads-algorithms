# Algorithms for Decision Support
Adam Smits, Frank Hoogmoed, Freek van der Heijden & Angel Temelko

## Usage
### Offline Algorithm
In the OfflineAlgorithm there are two parameters you can adjust:

- timeoutTime  (**Int**)  
    -   Amount of milliseconds before the program gives the best found solution and terminates.
- simpleInterface (**Bool**) 
    -   Using the simple interface that only outputs something after the timeout

### Online Algorithm
The only parameter to be changed for the online problem is the strategy used.
In the Console.WriteLine statement in the main method the strategy that is currently there can be changed to any of the other strategies listed in the commentblock above the WriteLine statement.

Below the scoring function for assigning seats can be specified.
Possible choices are:
- TopLeft
- SpaceScore
- OccupationScore
- SpaceOccupationScore
- Random3SpaceOccupationScore
- BestRandomSpaceOccupationScore

Any of those can be placed in the line below in the following format with ***FUNCTION*** replaced with the terms above:

Console.WriteLine(***FUNCTION***(occurencelist))